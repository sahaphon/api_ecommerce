const db = require("./db");

const authenticatedMiddleware = async (req, res, next) => {
    const authToken = req.headers.authorization
    if (authToken === 'undefined' || authToken === 'null' || !authToken) {
      return res.send({ success: false, msg: "no token" });
    }
    const [userID, token] = authToken.split(":");
    let strSql = `SELECT token FROM users WHERE userID = '${userID}' AND token ='${token}' `;
    let result = await db.sqlQuery(strSql);

    if (result.recordset.length !== 0) {
      next();
    } else {
      res.send({ success: false, msg: "you are not authenticated" });
    }
  };


  module.exports.authenticatedMiddleware =authenticatedMiddleware