const db = require("../db");
const moment = require("moment");

exports.ShoeDesc = async (req, res) => {

    //const { prodcode } = req.body;
    const prodcode = 'GS-82504M1';

    var strSql = ""
    strSql = `select prodcode, p_novat, property, description from product_prop where prodcode = '${prodcode}'`;

    const result1 = await db.sql_query(strSql)

    //select color per item solide
    strSql = `with p as (select prodcode, colorcode from shoed where n_pack = 2 and prodcode = '${prodcode}' group by prodcode, colorcode)
           ,n as (select colorcode, [desc] from acolor)
           ,img as (select prodcode, colorcode, image1 from product_info)
    
           select p.colorcode, n.[desc], img.image1 from p inner join n on p.colorcode = n.colorcode
               left join img on p.prodcode = img.prodcode and p.colorcode = img.colorcode`;

    const result2 = await db.sql_query(strSql)

    //select size per item
    //strSql = `select size from contrnd where tns = '1' and prodcode = '${prodcode}' group by size`;
      strSql =   `with a as (SELECT prodcode,packcode
 		  FROM shoed
		  WHERE prodcode = '${prodcode}' AND n_pack = 2  group by prodcode, packcode)
  
               ,b as (select packcode, [desc] as name, s_size, pairs from packsize)

               select a.packcode, b.name, b.s_size, b.pairs from a left join b on a.packcode = b.packcode`;
   
    const result3 = await db.sql_query(strSql)

    //select image slide per color
     strSql = `with a as (SELECT prodcode, colorcode, slide_img1, slide_img2, slide_img3, slide_img4, slide_img5
        FROM product_info
        WHERE prodcode =  '${prodcode}')

        ,b as (select colorcode, [desc] as name from acolor)

        select a.colorcode, b.name, a.slide_img1, a.slide_img2, a.slide_img3, a.slide_img4, a.slide_img5 from a inner join b on a.colorcode = b.colorcode`;   


    const result4 = await db.sql_query(strSql)


   //****************** Asort ************
      //select color per item asort
      strSql = `with p as (select prodcode, colorcode from shoed where n_pack = 1 and prodcode = '${prodcode}' group by prodcode, colorcode)
           ,n as (select colorcode, [desc] from acolor)
           ,img as (select prodcode, colorcode, image1 from product_info)
    
           select p.colorcode, n.[desc], img.image1 from p inner join n on p.colorcode = n.colorcode
               left join img on p.prodcode = img.prodcode and p.colorcode = img.colorcode`;

    const result5 = await db.sql_query(strSql)

     //select size per item asort

      strSql =   `with a as (SELECT prodcode,packcode
 		  FROM shoed
		  WHERE prodcode = '${prodcode}' AND n_pack = 1  group by prodcode, packcode)
  
               ,b as (select packcode, [desc] as name, s_size, pairs from packsize)

               select a.packcode, b.name, b.s_size, b.pairs from a left join b on a.packcode = b.packcode`;
   
    const result6 = await db.sql_query(strSql)

   

    ///********************************** */
    let newDt = result1.recordset.map(value=>{
        return {
            prodcode: value.prodcode,
            p_novat: value.p_novat,
            property: value.property,
            description: value.description,
            slide_img: result4.recordset,
            solid: [{
                    color: result2.recordset,
                    size: result3.recordset
                 }],
            asort: [{
                    color: result5.recordset,
                    size: result6.recordset
                 }]
        }
    })

    res.send(newDt);
};

