const db = require("../db");
const sql = require("mssql");
const crypto = require("crypto");
const moment = require("moment");
const randomString = require("randomstring"); 

exports.UserAll = async (req, res) => {
    const strsql = `SELECT * FROM [DBwebadda].[dbo].[usersv]`;
    const result = await db.sql_query(strsql);
    res.send({ status: true, data: result.recordset });
  };
  
exports.RewritSalt = async (req, res) => {
      
    const sql = `SELECT user_id, pass FROM user_cntrnd order by user_id`;
    const results = await db.sql_query(sql);
  
    if (results.recordset.length !== 0)
    {
       var i = 0
       for ( i == 0; i <= results.recordset.length - 1; i ++)
       {
          let rDS = randomString.generate({ length: 30, charset: "alphabetic" });
          let pass = crypto
            .createHmac("sha512", rDS)
            .update(results.recordset[i].pass)
            .digest("hex");
          const current = moment().format("YYYY-MM-DD HH:mm:ss");
  
          const strsql = `UPDATE user_cntrnd SET
                            salt = '${rDS}',
                            hpass = '${pass}'
                            WHERE user_id = '${results.recordset[i].user_id}'`;
        //   (user_id , password , salt , pfs_id , level , status , preby , predate)
  
          if (await db.sql_query(strsql)) 
          {
              console.log("Success : ", results.recordset[i].user_id)
              //res.send({ status: true });
          } else {
              console.log("error insert salt.")
             // res.send({ status: false });
           }
       }
    }
};

