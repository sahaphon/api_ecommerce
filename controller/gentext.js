const db = require("../db");
const moment = require("moment");

function random_ABC(length)
{
   var result = '';
   var charactors = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   var charactorsLength = charactors.length;

   for ( var i = 0; i < length; i++)
   {
       result += charactors.charAt(Math.floor(Math.random() * charactorsLength));
   }

   return result;
}


exports.genTxt = async (req, res) => {

    const { od } = req.body.od;
    const { user } = req.body.user;
    const { code } = req.body.code;

    var newDate = new Date();
    var time = Date.now();
    var dbfname = '';


    if (code.length == 4)
    {
        dbfname = code + random_ABC(4);
    }
    else if (code.length == 5)
    {
        dbfname = code + random_ABC(3);
    }
    else if (code.length == 6)
    {
        dbfname = code + random_ABC(2);
    }
    
    /****  SELECT TOP 1 ******/
    var SQL = `SELECT TOP 1 orderno FROM dbo.odnoh ORDER BY orderno DESC, predate DESC`;
    const result = await db.sql_query(SQL);
    
    var currOdno = result.recordset[0].orderno;

    var year = newDate.getFullYear();  //get current year  2019
    if (year < 2500)
    {
        year = year + 543;  ///2562
    }

    var currYear = moment(time).format("MM"); //201912
    var sYear = year.toString();   //62
    currYear = sYear.substr(2,2) + currYear;

    var run = currOdno.substr(6,4);  //OD201912XXXX  ==> XXXX

    if ( currYear == currOdno.substr(2,4))  //หากปีเดือนเดียวกัน
    {
         run = parseInt(run) + 1;
         var s = '0000' + run;

         currOdno = currOdno.substr(0,6) + s.substr(s.length - 4);
    }
    else
    {
        currOdno = 'OD' + currYear + "0001";
    }


    //******* UPDATE NEW OD */
    SQL = `INSERT INTO dbo.odnoh VALUES(
        '${currOdno}',
        '${dbfname}',
        '${code}',
        '${user}',
        '${moment(time).format("YYYY-MM-DD")}',
        '${moment(time).format("HH:mm:ss")}'
    )`;

    await db.sql_query(SQL);

    let data = od.map(value =>{
        return {
            date: value.date.substr(3,2) + '/'+ value.date.substr(0,2) + '/' + value.date.substr(6,4),
            time: moment(time).format("HH:mm"),
            duedate: value.delivery.substr(3,2) + '/'+ value.delivery.substr(0,2) + '/' + value.delivery.substr(6,4),          
            orderno: currOdno,
            code: value.code,
            no: parseInt(value.no),
            prodcode: value.prodcode,
            n_pack: parseInt(value.n_pack),
            colorcode: value.colorcode,
            qty: value.qty,
            price: 0,
            amt: 0,
            packcode: value.packcode,
            packno: parseInt(value.packno),
            pairs: value.pairs,
            dozen: value.dozen,
            disc1: value.disc,
            pono: value.pono.replace(',', ' '),
            tax_rate: value.tax_rate,
            vat_type: value.vat_type,
            tax_amt: value.tax_amt,
            net_amt: value.net_amt,
            cr_term: value.cr_term,
            saleman: value.saleman,
            remark: value.remark.replace(',', ' '),
            recfirm:value.recfirm === 1 ? "Y" : "N",
            incvat: value.incvat === 1 ? "Y" : "N",
            logis_code: value.logicode,
            logicode: value.logicode,
            ctrycode: value.ctrycode,
            store: value.store,
            preby: 'SALE/' + user,
            predate: moment(time).format("MM/DD/YYYY"),
            pretime: moment(time).format("HH:mm:ss"),
            logi_name: value.logi_name + '\r'
        }
    })

    const axios = require('axios');
    
    axios.post('http://localhost/KeyOrders/write_txt.php',{ od: data, id: currOdno})
    .then( response => {
    // console.log('Respuesta', response.data);
    })
    .catch( response => {
        res.send([{result:"0"}])
        console.log('Error', response);
    })
    .finally( () => {
        res.send([{result:"1"}])
        console.log('Finalmente...');
    });
    
};