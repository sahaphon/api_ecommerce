﻿const db = require("../db");
const crypto = require("crypto");
const moment = require("moment");

const genRandomString = length => {
    return crypto
      .randomBytes(Math.ceil(length / 2))
      .toString("hex") /** convert to hexadecimal format */
      .slice(0, length); /** return required number of characters */
  };


  const sha512 = (password, salt) => {
    let hash = crypto.createHmac("sha512", salt); /** Hashing algorithm sha512 */
    hash.update(password);
    let value = hash.digest("hex");
    return {
      salt: salt,
      passwordHash: value
    };
  };


  exports.login= async (req, res) => {

      const { userID, pass } = req.body;
      
      var strSql = `SELECT salt FROM users WHERE userID= '${userID}' AND sta_usr= 0`;
      var result = await db.sql_query(strSql);

      if (result.recordset.length !== 0)
      {
          strSql = `SELECT a.act_user, b.userID, b.prefix, b.name, b.lastname 
                     FROM users a 
                     inner join registration b  on a.userID = b.userID
                     WHERE a.userID = '${userID}'`;

          //strSql = `SELECT userID, prefix, name, lastname FROM registration WHERE userID= '${userID}'`; 
          result = await db.sql_query(strSql);

         if (result.recordset.length !== 0)
         {
            var token = genRandomString(20)

            strSql = `UPDATE users SET lastlogin='${moment().format("YYYY-MM-DD")}'
                    , isexist='1'
                    , token='${token}'
                    WHERE userid='${userID}'`;

              await db.sql_query(strSql);
              res.send({ success : true, data : result.recordset, token : token })
          }
          else 
          {
                res.send({ success: false });
          }

      }
      else //กรณี ล็อคอินครั้งแรก หรือ การสุ่มพาสเวิร์ส(มั่วเข้ามา)
      {
           res.send({ success: false })
      }

  };
