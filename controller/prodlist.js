const db = require("../db");
const moment = require("moment");

exports.prodlist = async (req, res) => {

    var SQL = ""

     //สินค้าแนะนำ
    SQL = `WITH a as (SELECT Top 10 prodcode, title, price FROM [DBecommerce].[dbo].[product_prop] WHERE category = '01' ORDER BY prodcode)

    ,img as (SELECT prodcode, image1 FROM
    (SELECT prodcode, image1, ROW_NUMBER() OVER (PARTITION BY prodcode ORDER BY colorcode) as rank FROM [DBecommerce].[dbo].[product_info]) q
     WHERE rank = 1)
    
     SELECT a.*, img.image1 FROM a INNER JOIN img 
     on a.prodcode = img.prodcode`;
    const result1 = await db.sql_query(SQL)

    //สินค้าขายดี
    SQL = `WITH a as (SELECT Top 10 prodcode, title, price FROM [DBecommerce].[dbo].[product_prop] WHERE category = '02' ORDER BY prodcode)

    ,img as (SELECT prodcode, image1 FROM
    (SELECT prodcode, image1, ROW_NUMBER() OVER (PARTITION BY prodcode ORDER BY colorcode) as rank FROM [DBecommerce].[dbo].[product_info]) q
     WHERE rank = 1)
    
     SELECT a.*, img.image1 FROM a INNER JOIN img 
     on a.prodcode = img.prodcode`;
    const result2 = await db.sql_query(SQL)

    //สินค้าทั้งหมด
    SQL = `WITH a as (SELECT Top 10 prodcode, title, price FROM [DBecommerce].[dbo].[product_prop] ORDER BY prodcode)

    ,img as (SELECT prodcode, image1 FROM
    (SELECT prodcode, image1, ROW_NUMBER() OVER (PARTITION BY prodcode ORDER BY colorcode) as rank FROM [DBecommerce].[dbo].[product_info]) q
     WHERE rank = 1)
    
     SELECT a.*, img.image1 FROM a INNER JOIN img 
     on a.prodcode = img.prodcode`;
    const result3 = await db.sql_query(SQL)


    res.send({ prod_info: result1.recordset, bestsaler: result2.recordset, allprod: result3.recordset })
};