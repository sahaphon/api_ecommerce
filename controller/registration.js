const db = require("../db");
const moment = require("moment");

var multer = require('multer');
const crypto = require("crypto");
const randomString = require("randomstring");  //npm 

exports.Registration = async (req, res) => {
    const user = req.body.user;
    const passw = req.body.password;
    const prefix = req.body.prefix;
    const name  = req.body.name;
    const lname = req.body.lname;
    const company = req.body.company;
    const phone = req.body.phone;
    const email = req.body.email;
    const birthday = moment(req.body.birthday).utc().format("YYYY-MM-DD");
    const address = req.body.address;
    const tambol = req.body.tambol;
    const amphur = req.body.amphur;
    const province = req.body.province;
    const country = req.body.country;
    const zipcode = req.body.zipcode;
    const ref1 = req.body.ref1;
    const ref2 = req.body.ref2;
    const ref3 = req.body.ref3;
    const ref4 = req.body.ref4;

     //Check userid && password exist
     let chkUser = `SELECT userID FROM registration WHERE userID = '${user}' OR email = '${email}'`;
     let result_us = await db.sql_query(chkUser);

     if (result_us.recordset.length === 0)
     {
           //Gen regis_id
            let sql =  `SELECT TOP 1 regno FROM registration ORDER BY regno DESC`;
            let result = await db.sql_query(sql);
        
            var regID = "";
            var newDate = new Date()
            var year = newDate.getFullYear();  //get current year
        
            if (result.recordset.length !== 0)
            {
                regID = result.recordset[0].regno   
        
                if (year > 2500)
                {
                    year = year - 543;
                }
                
                var strYear = year.toString();
                strYear = strYear.slice(2);      //ตัดสตริง
        
                if (regID.substr(1,2) == strYear)   //หากปีเดียวกัน
                {
                    let num = parseInt(regID.substr(3,4)) + 1;
        
                    // เติม 0 หน้าลำดับหมายเลข
                    var s = "000" + num;
                    regID = regID.substr(0,3) + s.substr(s.length - 4);
                } 
                else
                {
                    regID = "R" + strYear + "0001";
                }
            }
            else
            {
                var strYear = year.toString();
                strYear = strYear.slice(2);      //ตัดสตริง
        
                regID = "R" + strYear + "0001";
            }
            // console.log("ผ่าน")

            var currDate = Date.now();
            let insertSql = `INSERT INTO registration VALUES(
                            '${regID}',
                            '${user}',
                            '${prefix}',
                            '${name}',
                            '${lname}',
                            '${phone}',
                            '${email}',
                            '${birthday}',
                            '${company}',
                            '${address}',
                            '${tambol}',
                            '${amphur}',
                            '${province}',
                            '${country}',
                            '${zipcode}',
                            '${ref1}',
                            '${ref2}',
                            '${ref3}',
                            '${ref4}',
                            '${moment(currDate).format("YYYY-MM-DD HH:mm:ss")}',
                            0,
                            '',
                            ''
            )`;

            console.log(insertSql);
            await db.sql_query(insertSql);

            //******************วนลูปบันทึกไฟล์แนบ
            //     var i = 0;

            //     console.log("call ...")
            //     var storage = multer.diskStorage({
            //         destination: function (req, file, callback) {
            //             callback(null, '//10.32.0.14/onlinesale/doc') 
            //     },
            //     filename: function (req, file, callback) {
                  
            //         //var fname = Date.now() + '-' +file.originalname;
            //         var fname = regID + Date.now();
            //         var updateSql = ""

            //         if (i === 0)
            //         {
            //             updateSql= `UPDATE registration SET
            //                         ref1 = '${fname}',
            //                         WHERE userId = '${user}'`;
                        
            //             db.sql_query(updateSql)
            //         }
            //         else if (i === 1)
            //         {
            //              updateSql= `UPDATE registration SET
            //                         ref2 = '${fname}',
            //                         WHERE userId = '${user}'`;
                        
            //               db.sql_query(updateSql)
            //         }
            //         else if (i ===2)
            //         {
            //             updateSql= `UPDATE registration SET
            //                     ref3 = '${fname}',
            //                     WHERE userId = '${user}'`;
            
            //              db.sql_query(updateSql)
            //         }
            //         else
            //         {
            //             updateSql= `UPDATE registration SET
            //                     ref4 = '${fname}',
            //                     WHERE userId = '${user}'`;
            
            //              db.sql_query(updateSql)
            //         }

            //         //console.log("name : ", i + ' -' + file.originalname)
            //         //callback(null, file.originalname) //ไฟล์ต้นฉบับ
            //         callback(null, fname) //ใส่เวลานำหน้าไฟล์
            //         i = i + 1
            //     }
            // })
            
            // var upload = multer({ storage: storage }).array('file', 4)
            
            // upload(req, res, function (err) {
            //     if (err instanceof multer.MulterError) {
            //         return res.status(500).json(err)
            //     } else if (err) {
            //         return res.status(500).json(err)
            //     }
            // return res.status(200).send(req.file)
            
            // })

            //************** Loop Add user
                let rDS = randomString.generate({ length: 30, charset: "alphabetic" });
                let pass = crypto
                .createHmac("sha512", rDS)
                .update(passw)
                .digest("hex");

                //Insert new user
                const addSql =  `INSERT INTO users VALUES(
                               '${moment(currDate).format("YYYY-MM-DD HH:mm:ss")}',
                               'U',
                               '${user}',
                               '${rDS}',
                               '${pass}',
                               0,
                               0,
                               '',
                               '',
                               '',
                               ''
                )`;

                if (await db.sql_query(addSql)) 
                {
                    //console.log("add success..")
                    res.send({ success: true });
                } else {
                    //console.log("error insert salt.")
                    res.send({ success: false, exist: false });
                }

     }
     else
     {
         //user มีผู้ใช้งานก่อนแล้ว
         res.send({ success: false, exist: true })
     }
};