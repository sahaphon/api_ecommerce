﻿var multer = require('multer');

exports.fileupload = async (req, res) => {

	var storage = multer.diskStorage({
		destination: function (req, file, callback) {
			callback(null, 'uploads') 
	  },
	  filename: function (req, file, callback) {
		callback(null, Date.now() + '-' +file.originalname ) //ใส่เวลานำหน้าไฟล์
	  }
  })

  //var upload = multer({ storage: storage }).array('file', 4)
  var upload = multer({ storage: storage }).single('file')

  upload(req, res, function (err) {
	if (err instanceof multer.MulterError) {
		return res.status(500).json(err)
	} else if (err) {
		return res.status(500).json(err)
	}

   return res.status(200).send(req.file)

})

};
