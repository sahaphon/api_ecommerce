const db = require("../db");
const moment = require("moment");

exports.shoe_women = async (req, res) => {

    //รองเท้่าสตรี
    const SQL = `WITH a as (SELECT prodcode, title, price FROM [DBecommerce].[dbo].[product_prop] WHERE group_id = '02')

    ,img as (SELECT prodcode, image1 FROM
    (SELECT prodcode, image1, ROW_NUMBER() OVER (PARTITION BY prodcode ORDER BY colorcode) as rank FROM [DBecommerce].[dbo].[product_info]) q
     WHERE rank = 1)
    
     SELECT a.*, img.image1 FROM a INNER JOIN img 
     on a.prodcode = img.prodcode`;
    const result = await db.sql_query(SQL)

    res.send({ women: result.recordset })
};