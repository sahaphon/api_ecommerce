const sql = require("mssql");

const dbConfig = {
    user: "Sa",
    password: "Sa2008",
    server: "10.32.0.14",
    database: "DBecommerce"
};

const conn = new sql.ConnectionPool(dbConfig);
conn.connect();

exports.sql_query = (query) => {
    try {
        const request = new sql.Request(conn);
        const result = request.query(query);
        return result;
    } catch (err) {
        console.error("SQL error", err);
    }
};
