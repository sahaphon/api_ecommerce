const express = require("express");
const cors = require("cors");
const app = express();
const bodyParser = require("body-parser");
const db = require("./db");

const url = "/api";
const http = require("http").Server(app);

const server = http.listen(4002, function () {
    console.log("Server is running..");
});

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const auth = require("./authenticated");
const login = require("./routes/login");
// const file = require("./routes/files");
// const users = require("./routes/users");
const plist = require("./routes/prodlist");
const men = require("./routes/men");
const wmen = require("./routes/women");
const child = require("./routes/kids");
const upload = require("./routes/upload");
const add = require("./routes/registration");
const detail = require("./routes/description");


app.use(url + "/login", login);
app.use(url + "/prodlist", plist);
app.use(url + "/men", men);
app.use(url + "/women", wmen);
app.use(url + "/kids", child);
app.use(url + "/upload", upload);
app.use(url + "/register", add);
app.use(url + "/description", detail);

// app.use(url + "/files", auth.authenticatedMiddleware, file);
// app.use(url + "/users", auth.authenticatedMiddleware, users);