const express = require("express");
const router = express.Router();
const desc = require('../controller/description');

router.post("/", desc.ShoeDesc);

module.exports = router;