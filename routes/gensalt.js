const express = require("express");
const router = express.Router();
const gensalt = require('../controller/gensalt');

router.post("/", gensalt.RewritSalt);

module.exports = router;