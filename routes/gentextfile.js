const express = require("express");
const router = express.Router();
const gentxt = require('../controller/gentext');

router.post("/", gentxt.genTxt);

module.exports = router;