const express = require("express");
const router = express.Router();
const kids = require('../controller/kids');

router.post("/", kids.shoe_kids);

module.exports = router;