const express = require("express");
const router = express.Router();
const men = require('../controller/men');

router.post("/", men.shoe_men);

module.exports = router;