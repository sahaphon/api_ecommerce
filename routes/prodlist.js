const express = require("express");
const router = express.Router();
const plist = require('../controller/prodlist');

router.post("/", plist.prodlist);

module.exports = router;