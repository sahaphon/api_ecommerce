const express = require("express");
const router = express.Router();
const regis = require('../controller/registration');

router.post("/", regis.Registration);

module.exports = router;