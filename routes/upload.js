const express = require("express");
const router = express.Router();
const upload = require('../controller/upload');

router.post("/", upload.fileupload);

module.exports = router;