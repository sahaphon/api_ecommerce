const express = require("express");
const router = express.Router();
const wmen = require('../controller/women');

router.post("/", wmen.shoe_women);

module.exports = router;